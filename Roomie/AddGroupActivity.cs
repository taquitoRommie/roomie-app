using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Content.PM;
using Roomie.Models;

namespace Roomie
{
    [Activity(Label = "AddGroupActivity", ScreenOrientation = ScreenOrientation.Portrait)]
    public class AddGroupActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.Add_Group);
            Button addGroupButton = FindViewById<Button>(Resource.Id.addGroupButton);
            addGroupButton.Click += AddGroupButton_Click;
        }

        private async void AddGroupButton_Click(object sender, EventArgs e)
        {
            var GroupName = FindViewById<EditText>(Resource.Id.etGroupName);
            var Members = FindViewById<EditText>(Resource.Id.etMembers);
            var Description = FindViewById<EditText>(Resource.Id.etDescription);

            // TODO fix the users / members array thing 
            string response = await RestfulService.Post(new Group() { name = GroupName.Text, users = new string[] { Members.Text }, description = Description.Text }, "Group");
            Toast.MakeText(this, response, ToastLength.Long).Show();

            if (response != "ERROR")
            {
            Finish();
               // StartActivity(typeof(ViewGroupsActivity));//this will only happen after validated typically  
            }

        }
    }
}