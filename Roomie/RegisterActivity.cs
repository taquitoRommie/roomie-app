using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Text.RegularExpressions;
using Android.Content.PM;
using Roomie.Models;

namespace Roomie
{
    [Activity(ScreenOrientation = ScreenOrientation.Portrait)]
    public class RegisterActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.User_Registration);
            Button registerBut = FindViewById<Button>(Resource.Id.registerButton);
            registerBut.Click += RegisterBut_Click;
        }

        private async void RegisterBut_Click(object sender, EventArgs e)
        {
            

            bool success = false;//use to determine if registration was successful

            EditText userName = FindViewById<EditText>(Resource.Id.etUserName);
            EditText firstName = FindViewById<EditText>(Resource.Id.etFirstName);
            EditText lastName = FindViewById<EditText>(Resource.Id.etLastName);
            EditText email = FindViewById<EditText>(Resource.Id.etEmail);
            EditText password1 = FindViewById<EditText>(Resource.Id.etPassword1);
            EditText password2 = FindViewById<EditText>(Resource.Id.etPassword2);
            TextView validationMessage = FindViewById<TextView>(Resource.Id.tvValidation);

            if (firstName.Text == "")
            {
                validationMessage.Text = "Required First Name.";
            }
            else if (lastName.Text == "")
            {
                validationMessage.Text = "Required Last Name.";
            }
            else if (email.Text == "")
            {
                validationMessage.Text = "Required Email.";
            }
            else if (!Regex.IsMatch(email.Text,
                @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
                RegexOptions.IgnoreCase))
            {
                validationMessage.Text = "Email is not valid.";
            }
            else if (userName.Text == "")
            {
                validationMessage.Text = "Required Username.";
            }
            else if (password1.Text == "")
            {
                validationMessage.Text = "Required Password.";
            }
            else if (password2.Text == "")
            {
                validationMessage.Text = "Required Re-enter Password.";
            }
            else if (password2.Text.Length < 6)
            {
                validationMessage.Text = "Password minimum 6 characters long.";
            }
            else if (password2.Text != password1.Text)
            {
                validationMessage.Text = "Passwords dont match";
                password1.Text = "";
                password2.Text = "";
            }
            else
            {
                success = true;
                validationMessage.Text = "";//if this is the message then run through to server

            }

            //if server returns a success code
            if (success)
            {
                User user = new User
                {
                    username = userName.Text,
                    password = password1.Text,
                    firstName = firstName.Text,
                    lastName = lastName.Text,
                    email = email.Text
                };
                string response = await RestfulService.Post(user,"register");
                Toast.MakeText(this, response, ToastLength.Long).Show();
                Finish();
            }
            else
                Toast.MakeText(this, "Unable to register, please try again later.", ToastLength.Long).Show();
        }
    }
}