using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Content.PM;

namespace Roomie
{
    [Activity(Label = "SingleBillActivity", ScreenOrientation = ScreenOrientation.Portrait)]
    public class SingleBillActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Single_Bill);
            // Create your application here
        }
    }
}