using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using Android.Content.PM;

namespace Roomie
{
    [Activity(ScreenOrientation = ScreenOrientation.Portrait)]
    public class HomePageActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            

            SetContentView(Resource.Layout.Home_Page);
            LoadBalance();
            //create method to call that will load user balance here
            Button viewGroups = FindViewById<Button>(Resource.Id.groupsViewButton);
            viewGroups.Click += ViewGroups_Click;
        }

        void LoadBalance()//need to retrieve user balance here
        {
            var balTxt = FindViewById<TextView>(Resource.Id.balanceTxt);
            double bal = -99.989898989898989;
            bal = bal * 100;
            bal=Math.Truncate(bal);
            bal = bal / 100;
            if(bal>=0)
                balTxt.SetTextColor(Color.Green);
            else
                balTxt.SetTextColor(Color.Red);

            balTxt.Text = "$ "+bal.ToString();
          //textView.setTextColor(getResources().getColor(R.color.some_color)); once a resource file is up and running
        }
        private void ViewGroups_Click(object sender, EventArgs e)
        {
            StartActivity(typeof(ViewGroupsActivity));
        }

 

    }
}