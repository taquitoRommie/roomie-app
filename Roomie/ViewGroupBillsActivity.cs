using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Content.PM;

namespace Roomie
{
    [Activity(Label = "ViewGroupBillsActivity", ScreenOrientation = ScreenOrientation.Portrait)]
    public class ViewGroupBillsActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.View_Bills);
            // Create your application here
            //this will use view all bills axml to begin with as their format will be the same, if
            //any conflicts arise we'll need to create a new axml (just copy and rename all bills)
        }
    }
}