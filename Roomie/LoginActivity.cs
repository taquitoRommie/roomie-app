﻿using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Graphics;
using Android.Graphics.Drawables;
using System.Text.RegularExpressions;
using Android.Content.PM;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Roomie.Models;

namespace Roomie
{
    [Activity(MainLauncher = true, Icon = "@drawable/icon", ScreenOrientation = ScreenOrientation.Portrait)]
    public class LoginActivity : Activity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView(Resource.Layout.Login_Page);

            Button login = FindViewById<Button>(Resource.Id.loginButton);
            login.Click += Login_Click;

            TextView register = FindViewById<TextView>(Resource.Id.registerText);
            register.Click += Register_Click;       
        }
  
        private void Register_Click(object sender, EventArgs e)
        {
            StartActivityForResult(typeof(RegisterActivity), 0); 
        }

        private async void Login_Click(object sender, EventArgs e)
        {
            var pass = FindViewById<EditText>(Resource.Id.passText);
            var user = FindViewById<EditText>(Resource.Id.userText);

            string response = await RestfulService.Post(new User(){ username = user.Text, password = pass.Text }, "login");
            Toast.MakeText(this, response, ToastLength.Long).Show();
            if(response != "ERROR")
            {
                var userModel = JObject.Parse(response);
            
                // if (user.Text.Length == 0 || pass.Text.Length == 0)
                //use user and pass to validate credentials on server

                StartActivity(typeof(HomePageActivity));//this will only happen after validated typically  
            }
        }
    }
}

