using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Roomie.Models
{
    public class Group
    {
        public string name { get; set; }
        public string description { get; set; }
        // TODO rethink the styff being passed to server
        public string _id { get; set; }
        public object[] standings { get; set; }
        public DateTime createdOn { get; set; }
        public bool isActive { get; set; }
        public object[] bills { get; set; }
        public string[] users { get; set; }
    }

}