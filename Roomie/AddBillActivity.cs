using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Content.PM;

namespace Roomie
{
    [Activity(Label = "AddBillActivity", ScreenOrientation = ScreenOrientation.Portrait)]
    public class AddBillActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Add_Bill);

            // Create your application here
        }
    }
}