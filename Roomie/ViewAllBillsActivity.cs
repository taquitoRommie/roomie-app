using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Content.PM;

namespace Roomie
{
    [Activity(Label = "ViewAllBillsActivity", ScreenOrientation = ScreenOrientation.Portrait)]
    public class ViewAllBillsActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.View_Bills);
            // Create your application here
        }
    }
}