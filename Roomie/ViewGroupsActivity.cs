using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Content.PM;
using Android.Graphics;

namespace Roomie
{

 
    [Activity(ScreenOrientation = ScreenOrientation.Portrait)]
    public class ViewGroupsActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            
            SetContentView(Resource.Layout.View_All_Groups);//need to rename layouts
            LoadGroups();//will need to edit to accept data, intent.putData(str name, str data) will allow this then pull 
                         //data with string text = Intent.GetStringExtra ("name") ?? "Data not available";
                         //do all of this in the OnCreate
        }

        void LoadGroups()
        {

          
        
            var layout = FindViewById<GridLayout>(Resource.Id.gridLayout1);

            layout.TextAlignment = TextAlignment.Center;




            for (int i = 0; i < 25; ++i)
            {
                var newView = new GridLayout(this);
                newView.ColumnCount = 1;
                var aButton = new ImageButton(this);
                aButton.SetBackgroundColor(Color.ParseColor("#ff212121"));

                //aButton.Tag

                var txt = new TextView(this);
                txt.Text = "       Group " + i + 1;
                txt.TextAlignment = TextAlignment.Center;
                txt.Gravity = GravityFlags.Center;
                aButton.Id = i + 1;

                aButton.SetImageResource(Resource.Drawable.circular);



                aButton.Click += Group_Click;
                newView.AddView(aButton);
                newView.AddView(txt);

                layout.AddView(newView);
            }


            var newViews = new GridLayout(this);
            newViews.ColumnCount = 1;
            var aButtons = new ImageButton(this);
            aButtons.SetBackgroundColor(Color.ParseColor("#ff212121"));
            var txts = new TextView(this);
            txts.Text = "  Add Group ";
            txts.TextAlignment = TextAlignment.Center;
            txts.Gravity = GravityFlags.Center;


            aButtons.SetImageResource(Resource.Drawable.add);



            aButtons.Click += Add_Click;
            newViews.AddView(aButtons);
            newViews.AddView(txts);

            layout.AddView(newViews);


        }

        private void Group_Click(object sender, EventArgs e)
        {
            var c = sender as ImageButton;
            Intent viewGroup = new Intent(this, typeof(ViewSingleGroupActivity));
      
            viewGroup.PutExtra("Group_Id",c.Id.ToString());
            StartActivity(viewGroup);
            Toast.MakeText(this, "button " + c.Id + " clicked", ToastLength.Long).Show();
        }

        private void Add_Click(object sender, EventArgs e)
        {
            var c = sender as ImageButton;

            StartActivityForResult(typeof(AddGroupActivity), 0);
        }

    }
}