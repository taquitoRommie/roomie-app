using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Net.Http.Headers;

namespace Roomie
{
    public class RestfulService
    {
        //public RestfulService()
        //{
        //var authData = string.Format("{0}:{1}", Constants.Username, Constants.Password);
        //var authHeaderValue = Convert.ToBase64String(Encoding.UTF8.GetBytes(authData));

        //client = new HttpClient();
        //client.MaxResponseContentBufferSize = 256000;
        //client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authHeaderValue);
        //}

        public static async Task<string> Post(object postData, string endpoint)
        {

            HttpClient client = new HttpClient();
            client.MaxResponseContentBufferSize = 256000;

            //var uri = new Uri(string.Format(/*server*/Resource.RestfullIP()+"/register"));
            Uri uri = new Uri($"http://172.21.184.77:3000/{endpoint}/");
            string content = "";

            try
            {
                string jsonBody = JsonConvert.SerializeObject(postData);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = await client.PostAsync(uri, new StringContent(jsonBody, Encoding.UTF8, "application/json"));

                // TODO see if the post call was successful
                if (response.IsSuccessStatusCode)
                    content = await response.Content.ReadAsStringAsync();
                else
                {
                    content = "ERROR";
                }
            }
            catch (Exception ex)
            {
                content = ex.Message;
            }

            return content;
        }

        public static async Task<string> Get(string endpoint)
        {

            HttpClient client = new HttpClient();
            client.MaxResponseContentBufferSize = 256000;

            //var uri = new Uri(string.Format(/*server*/Resource.RestfullIP()+"/register"));
            Uri uri = new Uri($"http://172.21.184.77:3000/{endpoint}/");
            string content = "";

            try
            {
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = await client.GetAsync(uri);

                // TODO see if the post call was successful
                if (response.IsSuccessStatusCode)
                {
                    content = await response.Content.ReadAsStringAsync();
                }
                else
                {
                    content = "ERROR";
                }
            }
            catch (Exception ex)
            {
                content = ex.Message;
            }

            return content;
        }
    }

}